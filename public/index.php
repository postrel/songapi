<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 12:00
 */

require_once dirname(__DIR__) . '/autoload.php';

$router = new \Miniapi\app\Router(new \Miniapi\app\Request());
$app = new \Miniapi\app\Main($router);

$app->run();