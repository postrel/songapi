<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 14:34
 */

namespace Miniapi\app\Song;


use Miniapi\app\Response;
use Miniapi\app\User\UserController;

class SongController
{
    public function getSongById($id)
    {
        $user['song'] = '';
        $result       = $this->getModel()->selectSongById($id);
        if (is_array($result) && count($result)) {
            $user['song'] = $result[0];
        }
        $response = new Response($user);
        $response->withJson()->send();

    }

    public function getFavoritesByUserId($id)
    {
        $songs['favorites'] = $this->getModel()->selectFavoritesByUserId($id);
        $response = new Response($songs);
        $response->withJson()->send();

    }

    public function setFavoriteSongForUserId($songId, $userId)
    {
        $content = '';
        $response = new Response($content);
        if (!$this->checkSong($songId)) {
            $response->setContent(['response' => 'This song has not been written yet']);
            $response->withJson()->send();
        }
        if (!(new UserController())->checkUser($userId)) {
            $response->setContent(['response' => 'The user is unknown']);
            $response->withJson()->send();
        }

        if ($content = $this->getModel()->insertFavoriteSong($songId, $userId)) {
            $response->setContent(['response' => $content]);
            $response->withJson()->send();
        }
        
    }
    
    public function removeFavoriteSongForUserId($songId, $userId)
    {
        $content = '';
        $response = new Response($content);
        if (!$this->getModel()->isFavorite($songId, $userId)) {
            $response->setContent(['response' => "This isn't a user's favorite song"]);
            $response->withJson()->send();
        }

        if ($content = $this->getModel()->deleteFavoriteSong($songId, $userId)) {
            $response->setContent(['response' => $content]);
            $response->withJson()->send();
        }
        
    }

    public function checkSong($id)
    {
        if ($this->getModel()->selectSongById($id)) {
            return true;
        }

        return false;
        
    }

    private function getModel()
    {
        return new SongModel();
    }
}