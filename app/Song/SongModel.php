<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 14:35
 */

namespace Miniapi\app\Song;


use Miniapi\app\Model;
use PDO;

class SongModel extends Model
{
    public function selectSongById($id)
    {
        $query = 'SELECT * FROM songs
                  WHERE song_id = :id';

        $prep = $this->pdo->prepare($query);
        $prep->bindValue(':id', $id, PDO::PARAM_INT);

        $prep->execute();

        return $prep->fetchAll(PDO::FETCH_ASSOC);
    }

    public function selectFavoritesByUserId($id)
    {
        $query = 'SELECT t1.* 
                  FROM songs t1
                  LEFT JOIN favorites t2 ON (t1.song_id = t2.song_id)
                  WHERE t2.user_id = :id';

        $prep = $this->pdo->prepare($query);
        $prep->bindValue(':id', $id, PDO::PARAM_INT);

        $prep->execute();

        return $prep->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insertFavoriteSong($songId, $userId)
    {
        if ($this->isFavorite($songId, $userId)) {
            return "It's already a user's favorite song!";
        }

        $query = 'INSERT INTO favorites 
                  (user_id, song_id)
                  VALUES 
                  (:user_id, :song_id)
                  ';

        $prep = $this->pdo->prepare($query);
        $prep->bindValue(':user_id', $userId, PDO::PARAM_INT);
        $prep->bindValue(':song_id', $songId, PDO::PARAM_INT);

        $prep->execute();
        $prep->closeCursor();
        $prep = null;

        if($this->pdo->lastInsertId()){
            return 'Favorite song added';
        }
    }

    public function deleteFavoriteSong($songId, $userId)
    {
        $query = 'DELETE FROM favorites 
                  WHERE user_id = :user_id AND song_id = :song_id
                  ';

        $prep = $this->pdo->prepare($query);
        $prep->bindValue(':user_id', $userId, PDO::PARAM_INT);
        $prep->bindValue(':song_id', $songId, PDO::PARAM_INT);

        $result = $prep->execute();
        $prep->closeCursor();
        $prep = null;

        if ($result) {
            return 'It was not such a good song anyway.';
        }
    }

    public function isFavorite($songId, $userId)
    {

        $query = 'SELECT COUNT(*) 
                  FROM favorites t1
                  WHERE t1.user_id = :userId
                  AND t1.song_id = :songId
                  ';

        $prep = $this->pdo->prepare($query);
        $prep->bindValue(':userId', $userId, PDO::PARAM_INT);
        $prep->bindValue(':songId', $songId, PDO::PARAM_INT);

        $prep->execute();
        if ($prep->fetchColumn()) {
            return true;
        }

        return false;

    }

}