<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 17:04
 */

namespace Miniapi\app;


use PDO;

class DB
{
    private $pdo;

    private static $instance;

    private static $databaseType = 'mysql';
    private static $host = '127.0.0.1';
    private static $databaseName = 'songapi';
    private static $databaseLogin = 'songapi';
    private static $databasePassword = 'WH93394wz369zcgX6r9X';


    protected function __construct()
    {
        try {
            $this->pdo = new PDO(self::$databaseType . ':host=' . self::$host . ';dbname=' . self::$databaseName . ';charset=utf8mb4',
                self::$databaseLogin, self::$databasePassword);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    public static function getInstance()
    {
        if ( ! self::$instance ) {
            self::$instance = new self();
        }

        return self::$instance;

    }

    public function getPdo()
    {
        return $this->pdo;
    }

    private function __clone()
    {

    }

}