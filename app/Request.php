<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 14:59
 */

namespace Miniapi\app;


class Request
{
    private $method;
    private $requestUri;
    private $query;

    /**
     * Request constructor.
     */
    public function __construct()
    {
        if (isset($_SERVER['REQUEST_METHOD'])) {
            $this->method = $_SERVER['REQUEST_METHOD'];
        }
        if (isset($_SERVER['REQUEST_URI'])) {
            $this->requestUri = rtrim(explode('?',$_SERVER['REQUEST_URI'])[0], '/');
        }

        if (isset($_SERVER['QUERY_STRING'])) {
            $this->query = $_SERVER['QUERY_STRING'];
        }

    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return mixed
     */
    public function getRequestUri()
    {
        return $this->requestUri;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }
}