<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 14:23
 */

namespace Miniapi\app\User;


use Miniapi\app\Model;
use PDO;

class UserModel extends Model
{
    public function selectUserById($id)
    {
        $query = 'SELECT * FROM users
                  WHERE user_id = :id';
        
        $prep = $this->pdo->prepare($query);
        $prep->bindValue(':id', $id, PDO::PARAM_INT);

        $prep->execute();

        return $prep->fetchAll(PDO::FETCH_ASSOC);
    }

}