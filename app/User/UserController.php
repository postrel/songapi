<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 14:23
 */

namespace Miniapi\app\User;


use Miniapi\app\Response;

class UserController
{

    public function getUserById($id)
    {
        $user['user'] = '';
        $result =  $this->getModel()->selectUserById($id);
        if (is_array($result) && count($result)) {
            $user['user'] = $result[0];
        }
        $response = new Response($user);
        $response->withJson()->send();

    }

    private function getModel()
    {
        return new UserModel();
    }

    public function checkUser($id)
    {
        if ($this->getModel()->selectUserById($id)) {
            return true;
        }
        return false;
    }

}