<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 14:47
 */

namespace Miniapi\app;


class Main
{    
    private $router;

    /**
     * Main constructor.
     *
     * @param $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function run()
    {
        $this->router->dispatch();
    }


}