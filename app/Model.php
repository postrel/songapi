<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 17:02
 */

namespace Miniapi\app;


abstract class Model
{
    protected $pdo;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->pdo = DB::getInstance()->getPdo();
    }

}