<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 16:14
 */

namespace Miniapi\app;


use Miniapi\app\Song\SongController;
use Miniapi\app\Song\SongModel;
use Miniapi\app\User\UserController;
use Miniapi\app\User\UserModel;

/**
 * Class Router
 * @package Miniapi\app
 */
class Router
{
    /**
     * @var Request
     */
    private $request;

    /**
     * Router constructor.
     *
     * @param $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Defines handlers for requests
     */
    public function dispatch()
    {
        if ('GET' === $this->request->getMethod()) {
            $this->processGet($this->request);
        }
        if ('POST' === $this->request->getMethod()) {
            $this->processPost($this->request);
        }
        if ('PUT' === $this->request->getMethod()) {
            $this->processPut($this->request);
        }
        if ('DELETE' === $this->request->getMethod()) {
            $this->processDelete($this->request);
        }
    }

    /**
     * @param Request $request
     */
    private function processGet(Request $request)
    {
        if (preg_match('/^\/\buser\b\/(?P<userId>\d)$/', $request->getRequestUri(), $matches)) {
            (new UserController())->getUserById($matches['userId']);
        }

        if (preg_match('/^\/\bsong\b\/(?P<songId>\d)$/', $request->getRequestUri(), $matches)) {
            (new SongController())->getSongById($matches['songId']);
        }
        
        if (preg_match('/^\/\buser\b\/(?P<userId>\d)\/\bfavorites\b$/', $request->getRequestUri(), $matches)) {
            (new SongController())->getFavoritesByUserId($matches['userId']);
        }
    }

    /**
     * @param Request $request
     */
    private function processPost(Request $request)
    {
        if (preg_match('/^\/\buser\b\/(?P<userId>\d)\/\bfavorites\b\/(?P<songId>\d)$/', $request->getRequestUri(), $matches)) {
            (new SongController())->setFavoriteSongForUserId($matches['songId'], $matches['userId']);
        }

    }

    /**
     * @param Request $request
     */
    private function processPut(Request $request)
    {

    }

    /**
     * @param Request $request
     */
    private function processDelete(Request $request)
    {
        if (preg_match('/^\/\buser\b\/(?P<userId>\d)\/\bfavorites\b\/(?P<songId>\d)$/', $request->getRequestUri(), $matches)) {
            (new SongController())->removeFavoriteSongForUserId($matches['songId'], $matches['userId']);
        }

    }


}