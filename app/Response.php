<?php
/**
 * Created by Maxim Platonov.
 * User: Maxim
 * Date: 08/05/2016
 * Time: 18:14
 */

namespace Miniapi\app;


/**
 * Class Response
 * @package Miniapi\app
 */
class Response
{
    /**
     * Array of HTTP headers
     * @var array
     */
    private $headers = [];
    /**
     * content body
     * @var
     */
    private $content;

    /**
     * Response constructor.
     *
     * @param $content
     * @param array $headers
     */
    public function __construct($content, array $headers = [])
    {
        $this->headers = $headers;
        $this->content = $content;
    }

    /**
     * Sends an HTTP response
     * @return array
     */
    public function send()
    {
        if ( ! count($this->headers)) {
            return [];
        }
        foreach ($this->headers as $headerName => $headerValue) {
            header("$headerName: $headerValue");
        }
        echo (string)$this->content;
        exit;

    }

    /**
     * renders content body in JSON notation
     * @return $this
     */
    public function withJson()
    {
        $this->headers['Content-Type'] = 'application/json;charset=utf-8';
        $this->content                 = json_encode($this->content, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT);

        return $this;
    }

    /**
     * @param $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}