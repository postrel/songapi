Sujet 2 : Créer une mini API
==============================
Développez en PHP une mini API REST avec output en json
1. Mettez en place de quoi pouvoir récupérer les données d'un user en fonction de son id, et une
song en fonction de son id.
User doit renvoyer user_id, name, email
Song doit renvoyer song_id, title, duration
2. Cette api doit aussi être capable de renvoyer les musiques favorites d'un utilisateur. Elle doit
permettre de :
Récupérer
cette liste
Ajouter
une song dans cette liste
Retirer
une song de cette liste
En développant cette api, vous devez garder en tête qu'elle est susceptible d'évoluer
(nouveaux retours, nouveaux attributs dans les objets)
Vous avez à votre disposition une base mysql.

Intallation
-----------

* Remplacer les identifiants de connexion à la BDD dans
``app/DB.php``
* Faire pointer le domain de l'application web au dossier
``public``

Routes
------

``songapi.com/user/id``

``songapi.com/song/id``

``songapi.com/user/id/favorites``

``songapi.com/user/id/favorites/songId``

Usage
-----

### Récupérer les données d'un user

Envoyer un requête `GET` à ``songapi.com/user/id``

### Récupérer les données d'une song

Envoyer un requête `GET` à ``songapi.com/song/id``

### Récupérer les musiques favorites d'un utilisateur

Envoyer un requête `GET` à ``songapi.com/user/id/favorites``

### Ajouter une song dans la liste des musiques favorites d'un utilisateur

Envoyer un requête `POST` à ``songapi.com/user/id/favorites/songId``

### Retirer une song de cette liste

Envoyer un requête `DELETE` à ``songapi.com/user/id/favorites/songId``

